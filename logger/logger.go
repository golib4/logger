package logger

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"log"
	"time"
)

type Logger interface {
	Info(message string)
	Warning(message string)
	Error(message string)
	Infof(message string, args ...any)
	Warningf(message string, args ...any)
	Errorf(message string, args ...any)
	Clear(count int)
}

type elasticLogger struct {
	client *elastic.Client
	params Params
}

type Params struct {
	IndexName   string
	Source      string
	Environment string
}

func NewElasticLogger(client *elastic.Client, params Params) Logger {
	return &elasticLogger{
		client: client,
		params: params,
	}
}

type messageData struct {
	Timestamp   int64  `json:"timestamp"`
	Date        string `json:"dateTime"`
	Level       string `json:"level"`
	Message     string `json:"message"`
	Source      string `json:"source"`
	Environment string `json:"environment"`
}

func (l elasticLogger) Info(message string) {
	l.sendData(message, "INFO")
}

func (l elasticLogger) Infof(message string, args ...any) {
	l.sendData(fmt.Sprintf(message+" \n", args), "INFO")
}

func (l elasticLogger) Errorf(message string, args ...any) {
	l.sendData(fmt.Sprintf(message+" \n", args), "ERROR")
}

func (l elasticLogger) Error(message string) {
	l.sendData(message, "ERROR")
}

func (l elasticLogger) Warning(message string) {
	l.sendData(message, "WARNING")
}

func (l elasticLogger) Warningf(message string, args ...any) {
	l.sendData(fmt.Sprintf(message+" \n", args), "WARNING")
}

func (l elasticLogger) Clear(count int) {
	_, err := l.client.DeleteByQuery().
		Index(l.params.IndexName).
		Query(elastic.NewRangeQuery("@timestamp").Lt(
			time.Now().UTC().AddDate(0, 0, -7).Unix(),
		)).
		Size(count).
		Do(context.Background())
	if err != nil {
		log.Fatalf("Error executing the delete by query request: %s", err)
	}

	fmt.Println("Successfully deleted the first 1000 documents in the index.")
}

func (l elasticLogger) sendData(message string, level string) {
	go func() {
		_, err := l.client.Index().Index(l.params.IndexName).BodyJson(messageData{
			Timestamp:   time.Now().Unix(),
			Date:        time.Now().Format(time.RFC3339),
			Level:       level,
			Message:     message,
			Source:      l.params.Source,
			Environment: l.params.Environment,
		}).Do(context.Background())

		if err != nil {
			fmt.Printf("Error indexing log message: %v", err)
		}
	}()
}

type logger struct {
}

func NewOutputLogger() Logger {
	return logger{}
}

func (l logger) Info(message string) {
	println("INFO: " + message)
}

func (l logger) Infof(message string, args ...any) {
	println("INFO: " + fmt.Sprintf(message, args))
}

func (l logger) Errorf(message string, args ...any) {
	println("ERROR: " + fmt.Sprintf(message, args))
}

func (l logger) Error(message string) {
	println("ERROR: " + message)
}

func (l logger) Warning(message string) {
	println("WARNING: " + message)
}

func (l logger) Warningf(message string, args ...any) {
	println("WARNING: " + fmt.Sprintf(message, args))
}

func (l logger) Clear(count int) {
	println("Not implemented.")
}
