module gitlab.com/golib4/logger

go 1.22.2

require github.com/olivere/elastic/v7 v7.0.32

require (
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
